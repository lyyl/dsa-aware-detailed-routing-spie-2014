\documentclass[]{spie}

\usepackage{graphicx}
\usepackage{cite}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{url}
\usepackage{multirow}

\newcommand{\urlwofont}[1]
{
\urlstyle{same}\url{#1}
}

\title{DSA-Aware Detailed Routing for Via Layer Optimization}

\author{ Yuelin Du$^\dag$, Zigang Xiao$^\dag$, Martin D.F. Wong$^\dag$, He Yi$^\ddag$ and H.-S. Philip Wong$^\ddag$\\
 $^\dag$Dept. of Electrical and Computer Engineering, University of Illinois at Urbana-Champaign\\
 $^\ddag$Dept. of Electrical Engineering, Stanford University\\
 Email: \{du6, zxiao2, mdfwong\}@illinois.edu, \{heyi, hspwong\}@stanford.edu\\}

\begin{document}

\maketitle

\begin{abstract}

In detailed routing for integrated circuit (IC) designs, vias are usually randomly inserted in order to connect between different routing layers. In the 7 nm technology node and beyond, the wire pitch is below 40 nm, and consequently, the vias become very dense, making via layer printing a challenging problem. Recently block copolymer directed self-assembly (DSA) technology has demonstrated great advantages for via layer patterning using guiding templates. To pattern vias with DSA process, guiding templates are usually printed first with conventional lithography, $e.g.$, 193 nm immersion lithography (193i) that has a coarser pitch resolution. Then the guiding templates will guide the placement of the DSA patterns ($e.g.$, vias) inside, and these patterns have a finer resolution than the templates. Different template shapes have different control on the overlay accuracy of the inside vias. By performing DSA experiments, the guiding templates can be classified as feasible and infeasible templates according to the overlay requirement of the technology node. The templates that meet the overlay requirement are feasible templates, and other templates are infeasible. Without considering the DSA template constraints in detailed routing, randomly distributed vias may require infeasible templates to be patterned, which makes the via layers incompatible with the DSA process. In this paper, we propose a DSA-aware detail routing algorithm to optimize the via layers such that only feasible templates are needed for via layer patterning. In addition, among all the feasible templates, the one with better overlay accuracy has higher priority to be picked up by the router for via patterning, which further improves the yield. By enabling DSA process for via layer patterning in the 7 nm technology node, the proposed detailed routing strategy tremendously reduces the manufacturing cost and improves the throughput for IC fabrication. 

\end{abstract}

\keywords{DSA, Template, Detailed Routing, Via Layer Optimization}

\section{Introduction}

Due to the limitations of 2D layout printing for the advanced technology nodes, integrated circuit (IC) designers are moving towards a highly regular 1D gridded design style~\cite{smayling2008low}. Taking the 1D detailed routing as an example, only one routing direction is allowed in each routing layer. Whenever direction switching (vertical to horizontal or vice versa) is needed, a via has to be inserted in order to connect between different routing layers. While 1D routing is less flexible than 2D routing, the routing layers with strict 1D wires offer the advantages of larger process window and higher yield~\cite{liebmann2009simplify, smayling2008apf, bencher2009gridded}. Besides that, many advanced technologies have demonstrated their capabilities in printing 1D wires with very narrow pitch size, such as self-aligned double pattering (SADP) lithography~\cite{bencher200822nm}, self-aligned quadruple patterning (SAQP) lithography~\cite{xu2011sidewall} and directed self-assembly (DSA)~\cite{black2007polymer}. On the other hand, 1D detailed routing introduces a large amount of vias that are randomly distributed across the entire layout. In the 7 nm technology node, the via pitch can be as small as 40 nm. How to print the highly dense vias becomes a major challenge for the 7 nm technology node IC fabrication. 

The conventional 193 nm immersion (193i) lithography with single exposure has already reached its printability limit at the 28 nm technology node. In consequence, next generation lithography techniques have to be adopted for via layer printing in the 7 nm technology node and beyond, such as extreme ultraviolet lithography (EUV), electron beam lithography (Ebeam), multiple patterning lithography (MPL) and block copolymer directed self-assembly (DSA).  Despite years of study, EUV is still far from practical implementation, and E-beam can only be adopted for small volume production with low throughput. Double patterning lithography (DPL) is reaching its limit at the 22 nm node, and triple patterning lithography (TPL) is a natural extension of DPL targeting sub-14 nm node fabrication. However, with more masks involved, the manufacturing cost may become too high to be accepted. Recent research progress on DSA has shown this technique's strong potential for the contact/via layer manufacturing~\cite{stoykovich2007directed, yi2012contact, wong2012block, lai2013computational, du2013block}.

\begin{figure}[!h]
\centering
\includegraphics[width=\columnwidth]{fig/halfadder.pdf}\\
\caption{Comparison of DSA contact hole patterning between the 22 nm technology node and the 7 nm technology node with a half adder. In (c) and (d), the dark gray areas denote guiding templates and the black areas denote DSA contact holes. Scale bar: 200 nm.}\label{fig:halfadder}
\end{figure}

As shown in Fig.~\ref{fig:halfadder}, to use DSA for patterning irregularly positioned vias, topographical guiding templates are needed for block copolymer to form irregular patterns. At the 22 nm node (Fig.~\ref{fig:halfadder}(c)), for each via we can build a single hole guiding template so that a smaller DSA hole will form inside. With a smaller via pitch at the 7 nm technology node (Fig.~\ref{fig:halfadder}(d)), a larger template could be used to guide the formation of multiple DSA holes inside the template, corresponding to closely positioned vias in the layout. Different DSA patterns could be controlled by adjusting the shape, size and density of guiding templates~\cite{yi2012flexible}. However, the overlay accuracy of the vias may vary among different templates. Generally it is more difficult to control the overlay accuracy for a larger template. So far, the five templates shown in Fig.~\ref{fig:templates} are reported with reasonable overlay accuracy control~\cite{wong2012block, yi2013block}. Therefore, in this paper we only consider these five templates as feasible templates for via pattening. Note that with the development of the DSA process, the overlay accuracy can be more accurately controlled and some other template shapes may become feasible templates as well. Our proposed detailed routing algorithm can be easily expanded to consider other feasible template shapes. 

\begin{figure}[!h]
\centering
\includegraphics[width=\columnwidth]{fig/templates.pdf}\\
\caption{Feasible template shapes for via patterning.}\label{fig:templates}
\end{figure}

In addition, even among the feasible templates shown in Fig.~\ref{fig:templates}, the overlay control of the inside vias is still different, $e.g.$,  the single-hole template has better overlay accuracy than other feasible templates. We assign each template a cost value to describe its ability in overlay control. The template with better overlay accuracy has lower cost. Based on the feasible template shapes, we summarize all the feasible via patterns for DSA process in Fig.~\ref{fig:viapatterns}, where the cost of each pattern is equivalent to the cost of the corresponding template used to print it. Note that three vias in L-shape can be patterned by a four-hole template by inserting a dummy via, as shown in Fig.~\ref{fig:dummyvia}. Therefore, the cost of an L-shaped three-via pattern should be equal to the cost of a four-hole template. 

\begin{figure}[!h]
\centering
\includegraphics[width=0.8\columnwidth]{fig/viapatterns.pdf}\\
\caption{All the feasible via patterns that can be printed using DSA guiding templates.}\label{fig:viapatterns}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[width=0.5\columnwidth]{fig/dummyvia.pdf}\\
\caption{Three vias in L-shape can be patterned with a four-hole template by inserting a dummy via.}\label{fig:dummyvia}
\end{figure}

In conventional detailed routing, vias are usually randomly inserted by the router, which are very likely to compose infeasible via patterns, and any infeasible via pattern may cause the entire via layer to be incompatible with the DSA process. Due to the very limited freedom in 1D detailed routing, it is extremely difficult to remove all infeasible via patterns through post-routing processing. A more effective way for via layer optimization is to consider the DSA template constraints at the beginning of the detailed routing.

In this paper, we propose a DSA-aware detailed routing algorithm that takes consideration of the constraints on feasible via patterns for the DSA process. We guarantee that the via layers produced by our router can be successfully patterned using feasible DSA templates only. In addition, among all the feasible patterns, the proposed routing algorithm preferentially picks up lower-cost ones, which further improves the yield. For single net routing, we perform the Dijkstra's shortest path algorithm on the routing graph to obtain the optimal path. Overall, a negotiated congestion based routing scheme is developed, where the nets are routed sequentially, and wire crossing conflicts and infeasible via patterns are resolved over iterations of rip-up and reroute. As far as we know, this is the first work studying the DSA-aware detailed routing problem for via layer optimization. 

The rest of this paper is organized as follows. Section~\ref{sec:formulation} defines the overall DSA-aware detailed routing problem. Then the problem solution is proposed in Section~\ref{sec:solution}. Next, experimental results are reported in Section~\ref{sec:results}, and finally, Section~\ref{sec:conclusions} concludes the paper.

\section{Problem Definition}\label{sec:formulation}

In the previous section, we have demonstrated all feasible via patterns in Fig.~\ref{fig:viapatterns}. If any other pattern exists after detailed routing is completed, infeasible templates will be needed and the entire via layer is no longer compatible with the DSA process. Therefore, our primary objective is to only compose feasible patterns while inserting vias. On the other hand, it is mentioned previously that each feasible pattern is assigned with a cost value that describes the overlay accuracy. Since lower-cost patterns have better overlay accuracy that leads to higher yield, it is preferred to compose lower-cost patterns whenever possible. For example, suppose a total of three vias need to be inserted. It is preferred to pattern each via individually using three single-hole templates instead of patterning all vias together with a three-hole template. So our secondary objective is to preferentially compose lower-cost patterns. Based on the above analysis, we define the DSA-aware detailed routing problem as follows.

\newpage

\begin{definition}\label{def:stdOp}
\textbf{DSA-Aware Detailed Routing Problem}

\fbox{
\parbox{0.9\columnwidth}{
\textit{Given a set of feasible via patterns S, where each pattern has a cost value, detailed routing with via insertion is performed such that each produced via layer can be partitioned into a subset of patterns in S, subject to the constraint that vias on adjacent tracks (horizontally, vertically or diagonally) belong to the same pattern, and lower-cost patterns have higher priorities than higher-cost patterns to be composed by the inserted vias.}
}
}
\end{definition}

\section{Problem Solution}\label{sec:solution}

A negotiated congestion based scheme~\cite{mcmurchie1995pathfinder} is adopted in our DSA-aware detailed routing algorithm. In order to reduce the adverse effect of improper net ordering, wire crossing conflicts and infeasible via patterns are initially allowed, and then resolved over iterations of rip-up and reroute. The key subproblem of the negotiated congestion based routing scheme is how to perform maze routing for a single net on the routing graph in the presence of a set of previously routed nets. In this section, we first solve the key subproblem by adopting the shortest path algorithm on the routing graph; then we present the overall negotiated congestion based routing scheme. 

\subsection{Subproblem: Single Net Routing}

First, we convert the original routing grid into a routing graph $G=(V,E)$ by regarding every segment intersection as a vertex in $V$ and segments between vertices as edges in $E$, and each vertex is assigned with a cost value. We update the cost for a subset of vertices in $V$ whenever a net is successfully routed. Based on Fig.~\ref{fig:templates}, we denote the cost of a single-hole template (Fig.~\ref{fig:templates} (a)), a regular two-hole template (Fig.~\ref{fig:templates} (b)), a diagonal two-hole template (Fig.~\ref{fig:templates} (c)), a three-hole template (Fig.~\ref{fig:templates} (d)) and a four-hole template (Fig.~\ref{fig:templates} (e)) by $c_a$, $c_b$, $c_c$, $c_d$ and $c_e$ respectively, and let $c_i$ denote the cost of an infeasible template. Initially, since there are no vias existing in the layout, every vertex in the via layers has via cost $c_a$, as shown in Fig.~\ref{fig:costUpdate} (a). When a via is inserted by the previous net routed, its adjacent via cost is updated accordingly. As illustrated in Fig.~\ref{fig:costUpdate} (b), the grid filled in red denotes an inserted via by the previous net routed. Then if routing the current net results in a new via inserted at any one of the green grids, a regular two-hole via pattern will be composed. Otherwise if a via is inserted at any blue grid, a diagonal two-hole pattern will be composed. In consequence, the via cost of the corresponding vertices is updated accordingly. Similarly, in Fig.~\ref{fig:costUpdate} (c) where two vias have been inserted, a new via inserted at the yellow grid introduces a three-hole pattern, and  inserting another via at either of the black grids introduces an infeasible pattern. Note that in Fig.~\ref{fig:costUpdate} (d) where three vias already exist, another via inserted at either of the purple grids introduces an L-shaped three-hole via pattern that must be printed using a four-hole template. In consequence, the via cost of these two vertices is equivalent to a four-hole template cost. 

\begin{figure}[!h]
\centering
\includegraphics[width=\columnwidth]{fig/costUpdate.pdf}\\
\caption{The via cost of a subset of vertices is updated once vias are inserted by a net routed.}\label{fig:costUpdate}
\end{figure}

From Figure~\ref{fig:costUpdate} we can see that whenever a new via is inserted, it impacts the via cost of its neighboring vertices. Therefore, we update the cost values of all such vertices once a net is routed and vias are introduced. Then on the updated graph, the weighed sum $c_p = l_p + \alpha \times v_p$ is a good cost metric to minimize when routing a new net, where $l_p$ and $v_p$ denote the wire length cost and total via cost produced by the path $p$ computed respectively, and $\alpha$ is a user-defined parameter that specifies the relative importance between them. To minimize $c_p$, we perform the Dijkstra's shortest path algorithm to find the optimal path $p$ that connects the source and the target pins.

\subsection{Overall Routing Scheme}

In this subsection, we present the overall negotiated congestion based routing scheme for the DSA-aware detailed routing problem. We let the nets negotiate for routing resources by adding history cost to all vertices in $V$, and resolve wire crossing conflicts and infeasible via patterns over iterations of rip-up and reroute. The cost of a vertex $v$ is computed by the following formula:

\[
cost(v) = \alpha \times h_c \times n_c + \beta \times h_i
\]
where $h_c$ denotes the history cost for a wire crossing conflict, $n_c$ denotes the number of pre-routed nets having crossing conflict with the current vertex, and $h_i$ denotes the history cost for infeasible via patterns. All history cost is initialized as $1$. The scheme works as follows. We first route all nets sequentially in a random order on $G$. Then as long as wire crossing conflicts or infeasible via patterns exist, iterations of rip-up and reroute will be performed. When a net $i$ is ripped-up and rerouted, we first remove its current route, and update the cost values on the vertices it impacts. Then the shortest path algorithm is performed to compute a new path for net $i$, and newly impacted vertices are updated accordingly. In addition, if the new path causes any conflicts with previously routed nets, the history cost ($i.e.$, $h_c$ or $h_i$) on the corresponding vertices is incremented by $1$. In this way, the conflicting vertices grow more expensive over iterations, and those nets with more options will tend to choose alternative routes in subsequent iterations, so that the conflicts can potentially be resolved. In our implementation, this procedure will terminate when either no wire crossing conflict or infeasible via pattern exists, or enough iterations have been performed.

\subsection{Special Challenge for DSA-Aware Detailed Routing}

While routing a single net, it is very likely that more than one via is inserted by the shortest path $p$ computed. The Dijkstra's shortest path algorithm is performed on a fixed graph where the cost of vertices is determined by the previously routed nets, and hence it is unable to consider the conflicts between the newly inserted vias introduced by $p$. For example, suppose net $1$ is pre-routed, occupying two vias $a$ and $b$ as shown in Fig.~\ref{fig:special} (a). When routing net $2$, individually occupying via $c$ or via $d$ is allowed, since it either generates a three-hole template in Fig~\ref{fig:special} (b) or a single-hole and a two-hole templates in Fig.~\ref{fig:special} (c). However, if both via $c$ and via $d$ are occupied simultaneously, it results in an infeasible template in Fig.~\ref{fig:special} (d), which is not allowed. 

\begin{figure}[!h]
\centering
\includegraphics[width=\columnwidth]{fig/special.pdf}\\
\caption{The shortest path algorithm cannot handle via pattern conflicts introduced by the current path.}\label{fig:special}
\end{figure}

This problem can be automatically resolved by the proposed negotiated congestion based routing scheme. As mentioned in the previous subsection, infeasible patterns are allowed first while computing the shortest path $p$ for single net routing. However, while tracing back $p$, the infeasible patterns are captured and the history cost $h_i$ for all vias involved in those patterns is increased. For example, in Fig~\ref{fig:special} (d), when tracing back net $2$, an infeasible four-hole pattern consisting of via $a$, $b$, $c$ and $d$ is captured, and hence the history cost $h_i$ of the corresponding vertices is increased incrementally. By this means, the locations where infeasible patterns have existed become more expensive for via insertion in subsequent iterations, so that the infeasible patterns can potentially be resolved over iterations.

\section{Experimental Results}\label{sec:results}

We implement our algorithm in C++ on a Linux machine with 1.7GHz CPU and 4GB RAM. Figure~\ref{fig:result} shows a toy example that compares the via layer produced by a conventional router and our proposed DSA-aware router. The initial input netlist is provided in Fig.~\ref{fig:result} (a). Then Fig.~\ref{fig:result} (b) illustrates the results of conventional detailed routing. From Fig.~\ref{fig:result} (b) we can see that three infeasible via patterns are introduced inside the dashed red circles, meaning that the eleven conflicting vias are unprintable using feasible DSA templates. Hence, the via layer is not compatible with the DSA process. The routing results produced by the DSA-aware detailed router are shown in Fig.~\ref{fig:result} (c), where no infeasible via pattern exists, and in consequence, the entire via layer can be patterned properly using feasible DSA templates only.

\begin{figure}[!h]
\centering
\includegraphics[width=\columnwidth]{fig/result.pdf}\\
\caption{The comparison between conventional detailed routing and DSA-aware detailed routing performed on a toy netlist.}\label{fig:result}
\end{figure}

We then perform the experiments on a set of benchmarks with different netlist sizes, and the experimental results are shown in Table~\ref{tab:experiment}. The second column shows the number of nets in each test case. Then each pair of the remaining columns compares the conventional routing results and the DSA-aware routing results in terms of total number of vias, the number of conflicting vias, total wire length, required routing area and runtime, respectively. 

\begin{table}[!h]
\centering
\caption{Comparison between conventional detailed routing and DSA-aware detailed routing}\label{tab:experiment}
\begin{tabular}{|c|c||c|c|c|c|c|c|c|c|c|c|} \hline
Test & \multirow{2}{*}{$\sharp$ Net} &  \multicolumn{2}{|c|}{$\sharp$ Vias} & \multicolumn{2}{|c|}{$\sharp$ Conf. Vias} & \multicolumn{2}{|c|}{Wire Length ($\mu m$)} & \multicolumn{2}{|c|}{Area ($\mu m^2$)} & \multicolumn{2}{|c|}{Runtime(s)}\\
\cline{3-12}
Cases& & Conv. & DSA. & Conv. & DSA. & Conv. & DSA. & Conv. & DSA. & Conv. & DSA.\\ \hline
test1 & 1k & 2328 & 2168 & 1036 & 0 & 429.64 & 447.08 & 27.04 & 40.96 & 7 & 11\\ \hline
test2 & 2k & 4552 & 4272 & 1746 & 0 & 882.52 & 897.28 & 64 & 84.64 & 19 & 29\\ \hline
test3 & 3k & 6654 & 6360 & 2435 & 0 & 1310.76 & 1349.92 & 100 & 144 & 45 & 57\\ \hline
test4 & 4k & 8818 & 8582 & 2587 & 0 & 1758.68 & 1801.64 & 144 & 174.24 & 60 & 102\\ \hline
test5 & 5k & 11122 & 10904 & 3639 & 0 & 2202.64 & 2259.28 & 174.24 & 196 & 99 & 174\\ \hline
test6 & 6k & 13640 & 13316 & 5298 & 0 & 2646.8 & 2737.88 & 184.96 & 219.04 & 232 & 354\\ \hline
\end{tabular}
\end{table}

Compared to the conventional router, the DSA router inserts fewer vias for each test case, and all those vias can be patterned by feasible DSA templates. In other words, the via layers produced by our proposed router are completely compatible with the DSA process. Even though the wire length cost, the required routing resources and the runtime for the DSA router are slightly worse than the conventional router, enabling DSA process for via layer patterning in the 7 nm technology node will tremendously reduce the manufacturing cost and improve the throughput for IC fabrication.

\section{Conclusions}\label{sec:conclusions}

This paper proposes a DSA-aware detailed routing algorithm for via layer optimization. The DSA template constraints are considered while inserting vias during the detailed routing process, such that the produced via layers can be fully patterned by feasible DSA templates. The proposed work enables DSA process for via layer patterning in the 7 nm technology node, which tremendously reduces the manufacturing cost and improves the throughput for IC fabrication.

\section{Acknowledgement}\label{sec:acknowledge}

This work is supported in part by the National Science Foundation under grants CCF-1017516 and CCF-1320585, the National Science Foundation Directorate for Materials Research (DMR, CMMI, No. 1029452), the Global Research Collaboration (GRC) of the Semiconductor Research Corporation (SRC), and the W.M. Keck Foundation through the W.M. Keck Faculty Scholar Award. DSA experiments were performed at the Stanford Nanofabrication Facility (a node of the NSF funded NNIN) and the Stanford Nano Center (SNC).

\bibliographystyle{IEEE_ECE}

\bibliography{mybib}

\end{document}
